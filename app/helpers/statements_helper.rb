module StatementsHelper
  def get_ride(number)
    case number
    when 1 then 'Aller'
    when 2 then 'Retour'
    when 3 then 'Aller/Retour'
    end
  end
end
